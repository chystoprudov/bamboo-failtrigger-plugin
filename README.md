# README #

Plugin for Bamboo which add trigger to execute some plan when another plan is failed. It can be used to execute some cleanup 

### User instructions ###
After installing new trigger "After plan fail" will be available at plan triggers. Choose what plan failure should trigger your plan and click Save button.

### Troubleshooting ###
**Problem**: trigger configured but when plan fails it doesn't trigger anything.

**Investigation**: Enable DEBUG level logs for class com.atlassianlab.bamboo.plugins.failtrigger.FailedPlanEventListener (see https://confluence.atlassian.com/bamboo/logging-in-bamboo-289277239.html for instructions).
Then see in Bamboo server logs:

````
Found that TEST-PROD failed, looking if any plan depends on it
...
Found TEST-CONS expects failure of TEST-PROD
...
Found 1 plans to be triggered
...
Try to start TEST-CONS
````

### Compatilibity with Bamboo 5.9 ###
Bamboo has some limitations in API on execution build from plugins. It was solved in Bamboo 5.10.0

To make this plugin functional in Bamboo > 5.8 and < 5.10 you need to patch your Bamboo instance. Modify file applicationContextManagers.xml in <BAMBOO_INSTALL_DIR>/atlassian-bamboo/WEB-INF/classes folder
Replace

```
#!xml

 <bean id="buildDetectionActionFactory" class="com.atlassian.bamboo.build.BuildDetectionActionFactoryImpl">
```
with
```
#!xml

 <bean id="buildDetectionActionFactory" class="com.atlassian.bamboo.build.BuildDetectionActionFactoryImpl" plugin:available="true">
```
and restart Bamboo.

Workaround was tested on Bamboo 5.9.4


### Who do I talk to? ###

* Alexey Chystoprudov, achystoprudov@atlassian.com