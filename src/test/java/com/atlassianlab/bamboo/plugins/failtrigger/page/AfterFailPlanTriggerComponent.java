package com.atlassianlab.bamboo.plugins.failtrigger.page;

import com.atlassian.bamboo.pageobjects.pages.triggers.AbstractTriggerComponent;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

public class AfterFailPlanTriggerComponent extends AbstractTriggerComponent {
    @Inject
    private PageElementFinder finder;

    public AfterFailPlanTriggerComponent(final WebDriver driver,
                                         final Timeouts timeouts,
                                         final String formName) {
        super(driver, timeouts, formName);
    }

    public void setPlan(PlanKey parentPlanKey) {
        final SelectElement parentPlanSelect = finder.find(By.id("failtrigger_plan"), SelectElement.class);
        parentPlanSelect.getAllOptions().stream()
                .filter(option -> option.value().equals(parentPlanKey.getKey()))
                .forEach(parentPlanSelect::select);
    }
}
