package it.com.atlassianlab.bamboo.plugins.failtrigger;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.ConfigurePlanTriggersPage;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.webdriver.BambooWebDriverTest;
import com.atlassianlab.bamboo.plugins.failtrigger.page.AfterFailPlanTriggerComponent;
import com.atlassian.bamboo.testutils.TestBuildDetailsBuilder;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.user.TestUser;
import org.junit.Test;

import static org.junit.Assert.fail;

public class FailTriggerTest extends BambooWebDriverTest {
    @Override
    protected void onAfter() {

    }

    @Test
    public void childBuildTriggeredAfterFailOfParentPlan() throws Exception
    {
        TestBuildDetails parentPlan = new TestBuildDetailsBuilder()
                .withManualBuild()
                .withNoInitialBuild()
                .withScriptTask("exit 1")
                .build();
        backdoor.plans().createPlan(parentPlan);
        //create child plan
        TestBuildDetails childPlan = new TestBuildDetailsBuilder()
                .withManualBuild()
                .withNoInitialBuild()
                .withScriptTask("echo \"hello world\"")
                .build();
        backdoor.plans().createPlan(childPlan);
        //add dependency
        product.fastLogin(TestUser.ADMIN);
        addDependency(parentPlan.getKey(), childPlan.getKey());
        //trigger parent plan and fail it
        backdoor.plans().triggerBuildAndAwaitCompletion(parentPlan.getKey());
        //wait triggering of child plan
        backdoor.plans().waitForCompletedBuild(childPlan.getKey(), 1);
    }

    @Test
    public void childBuildNotTriggeredAfterSuccessOfParentBuild() throws Exception
    {
        TestBuildDetails parentPlan = new TestBuildDetailsBuilder()
                .withManualBuild()
                .withNoInitialBuild()
                .withScriptTask("echo \"hello world\"")
                .build();
        backdoor.plans().createPlan(parentPlan);
        //create child plan
        TestBuildDetails childPlan = new TestBuildDetailsBuilder()
                .withManualBuild()
                .withNoInitialBuild()
                .withScriptTask("echo \"hello world\"")
                .build();
        backdoor.plans().createPlan(childPlan);
        //add dependency
        product.fastLogin(TestUser.ADMIN);
        addDependency(parentPlan.getKey(), childPlan.getKey());
        //trigger parent plan and fail it
        backdoor.plans().triggerBuildAndAwaitCompletion(parentPlan.getKey());
        //wait triggering of child plan
        try
        {
            backdoor.plans().waitForCompletedBuild(childPlan.getKey(), 1);
            fail("Plan was triggered, but not expected. Parent key "
                    + parentPlan.getKey() + ", child key - " + childPlan.getKey());
        }
        catch (AssertionError e)
        {
            //it's expected
        }
    }

    private void addDependency(PlanKey parentPlanKey, PlanKey childPlanKey)
    {
        final ConfigurePlanTriggersPage triggersPage = product.visit(ConfigurePlanTriggersPage.class, childPlanKey);
        final AfterFailPlanTriggerComponent triggerComponent = triggersPage.addTrigger("After plan fail",
                AfterFailPlanTriggerComponent.class);
        triggerComponent.setPlan(parentPlanKey);
        triggerComponent.save();
    }
}
