package com.atlassianlab.bamboo.plugins.failtrigger;

import com.atlassian.bamboo.build.BuildDetectionAction;
import com.atlassian.bamboo.build.BuildDetectionActionFactory;
import com.atlassian.bamboo.chains.ChainExecution;
import com.atlassian.bamboo.event.ChainCompletedEvent;
import com.atlassian.bamboo.plan.NonBlockingPlanExecutionService;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.trigger.TriggerDefinition;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.event.api.EventListener;

import com.google.common.collect.Iterables;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FailedPlanEventListener {
    private static final String PLUGIN_KEY = "com.atlassianlab.bamboo.plugins.bamboo-fail-trigger:afterFailedPlan";
    private final NonBlockingPlanExecutionService nonBlockingPlanExecutionService;
    private final CachedPlanManager planManager;
    private final BuildDetectionActionFactory buildDetectionActionFactory;

    private static final Logger log = Logger.getLogger(FailedPlanEventListener.class);

    public FailedPlanEventListener(NonBlockingPlanExecutionService nonBlockingPlanExecutionService,
                                   CachedPlanManager planManager,
                                   BuildDetectionActionFactory buildDetectionActionFactory) {
        this.nonBlockingPlanExecutionService = nonBlockingPlanExecutionService;
        this.planManager = planManager;
        this.buildDetectionActionFactory = buildDetectionActionFactory;
    }

    @EventListener
    public void onChainCompleted(final ChainCompletedEvent event) {
        final ChainExecution chainExecution = event.getChainExecution();
        ImmutableChain trigger = planManager.getPlanByKey(chainExecution.getPlanResultKey().getPlanKey(), ImmutableChain.class);
        if (chainExecution.isFailed() && trigger != null) {
            log.debug("Found that " + trigger.getPlanKey() + " failed, looking if any plan depends on it");
            final BuildContext completedBuildContext = event.getBuildContext();
            Iterable<ImmutableTopLevelPlan> internalKey = findTriggerables(trigger.getPlanKey());
            log.debug("Found " + Iterables.size(internalKey) + " plans to be triggered");
            for (ImmutableTopLevelPlan plan : internalKey) {
                BuildDetectionAction triggeringAction = buildDetectionActionFactory.createDependentBuildDetectionAction(plan, trigger, completedBuildContext);
                log.debug("Try to start " + plan.getPlanKey());
                nonBlockingPlanExecutionService.tryToStart(plan, triggeringAction);
            }
        }
    }

    /**
     * Search for plans which should be triggered by failure of failedPlanKey
     *
     * @param failedPlanKey
     * @return
     */
    @NotNull
    private Iterable<ImmutableTopLevelPlan> findTriggerables(@NotNull final PlanKey failedPlanKey) {
        return planManager.getPlans().stream()
                .filter(shouldBeTriggered(failedPlanKey))
                .collect(Collectors.toList());
    }

    @NotNull
    private Predicate<ImmutableTopLevelPlan> shouldBeTriggered(@NotNull PlanKey failedPlanKey) {
        return input -> {
            final List<TriggerDefinition> triggerDefinitions = input.getTriggerDefinitions();
            for (final TriggerDefinition triggerDefinition : triggerDefinitions) {
                if (triggerDefinition.isEnabled()
                        && triggerDefinition.getPluginKey().equals(PLUGIN_KEY)
                        && triggerDefinition.getConfiguration().containsKey(FailedPlanTriggerConstants.SELECTED_PLAN_ATTRIBUTE)) {
                    final String planKey = triggerDefinition.getConfiguration().get(FailedPlanTriggerConstants.SELECTED_PLAN_ATTRIBUTE);
                    if (log.isDebugEnabled()) {
                        log.debug("Found " + input.getPlanKey() + " expects failure of " + planKey);
                    }
                    if (failedPlanKey.toString().equals(planKey)) {
                        return true;
                    }
                }
            }
            return false;
        };
    }
}