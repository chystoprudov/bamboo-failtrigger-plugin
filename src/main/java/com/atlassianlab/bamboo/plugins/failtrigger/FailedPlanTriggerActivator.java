package com.atlassianlab.bamboo.plugins.failtrigger;

import com.atlassian.bamboo.trigger.TriggerActivator;
import com.atlassian.bamboo.trigger.TriggerDefinition;
import com.atlassian.bamboo.trigger.Triggerable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class FailedPlanTriggerActivator implements TriggerActivator
{
    @Override
    public void initAndActivate(@NotNull Triggerable triggerable, @NotNull TriggerDefinition triggerDefinition, @Nullable Date date)
    {
        activate(triggerable, triggerDefinition);
    }

    @Override
    public void activate(@NotNull Triggerable triggerable, @NotNull TriggerDefinition triggerDefinition)
    {

    }

    @Override
    public void deactivate(@NotNull Triggerable triggerable, @NotNull TriggerDefinition triggerDefinition)
    {

    }
}
