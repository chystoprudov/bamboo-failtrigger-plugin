package com.atlassianlab.bamboo.plugins.failtrigger;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.trigger.TriggerConfigurator;
import com.atlassian.bamboo.trigger.TriggerDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.ww2.actions.chains.admin.triggers.TriggerUIConfigBean;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;


public class FailedPlanTriggerConfigurator implements TriggerConfigurator
{
    private static final String PLANS_ATTRIBUTE = "availablePlans";

    private I18nResolver i18nResolver;
    private CachedPlanManager planManager;

    // -------------------------------------------------------------------------------------------------- Public Methods
    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        addPlansToContext(context);
    }

    private void populateContextForAllOperations(@NotNull final Map<String, Object> context, @NotNull final TriggerDefinition triggerDefinition)
    {
        addPlansToContext(context);
        context.put(FailedPlanTriggerConstants.SELECTED_PLAN_ATTRIBUTE, triggerDefinition.getConfiguration().get(FailedPlanTriggerConstants.SELECTED_PLAN_ATTRIBUTE));
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TriggerDefinition triggerDefinition)
    {
        populateContextForAllOperations(context, triggerDefinition);
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TriggerDefinition triggerDefinition)
    {
        populateContextForAllOperations(context, triggerDefinition);
    }

    @Override
    public void validate(@NotNull final ActionParametersMap actionParametersMap, @NotNull final ErrorCollection errorCollection)
    {
        String selectedPlan = actionParametersMap.getString(FailedPlanTriggerConstants.SELECTED_PLAN_ATTRIBUTE);
        if (StringUtils.isEmpty(selectedPlan))
        {
            errorCollection.addError(FailedPlanTriggerConstants.SELECTED_PLAN_ATTRIBUTE, i18nResolver.getText("failtrigger.plan.error.required"));
        }
    }

    @NotNull
    @Override
    public Map<String, String> generateTriggerConfigMap(@NotNull final ActionParametersMap actionParametersMap, @Nullable final TriggerDefinition previousTriggerDefinition)
    {
        Map<String, String> config = Maps.newHashMap();
        String selectedPlan = actionParametersMap.getString(FailedPlanTriggerConstants.SELECTED_PLAN_ATTRIBUTE);
        config.put(FailedPlanTriggerConstants.SELECTED_PLAN_ATTRIBUTE, selectedPlan);
        return config;
    }

    @NotNull
    @Override
    public RepositorySelectionMode getRepositorySelectionMode()
    {
        return RepositorySelectionMode.NONE;
    }

    private void addPlansToContext(@NotNull Map<String, Object> context)
    {
        final List<ImmutableTopLevelPlan> plans = planManager.getPlans();
        if (context.containsKey(TriggerUIConfigBean.CTX_CHAIN))
        {
            final ImmutableChain currentPlan = (ImmutableChain) context.get(TriggerUIConfigBean.CTX_CHAIN);
            Iterables.removeIf(plans, input -> currentPlan.getPlanKey().equals(input.getPlanKey()));

        }
        context.put(PLANS_ATTRIBUTE, plans);
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    public void setI18nResolver(final I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }

    public void setPlanManager(CachedPlanManager planManager)
    {
        this.planManager = planManager;
    }
}
