package com.atlassianlab.bamboo.plugins.failtrigger;

public interface FailedPlanTriggerConstants
{
    String SELECTED_PLAN_ATTRIBUTE = "failtrigger.plan";
}
