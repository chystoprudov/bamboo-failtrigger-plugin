package tutorial;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.builders.notification.PlanStatusChangedNotification;
import com.atlassian.bamboo.specs.builders.notification.UserRecipient;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.MavenTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.ScheduledTrigger;
import com.atlassian.bamboo.specs.model.task.ScriptTaskProperties;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.MapBuilder;

@BambooSpec
public class PlanSpec {

    private Plan plan() {
        return new Plan(new Project()
                .key(new BambooKey("ZA"))
                .name("ZZ Alexey"),
                "Fail trigger plugin",
                new BambooKey("FTP"))
                .description("https://marketplace.atlassian.com/plugins/com.atlassianlab.bamboo.plugins.bamboo-fail-trigger/server/overview")
                .pluginConfigurations(new AllOtherPluginsConfiguration()
                        .configuration(new MapBuilder()
                                .put("custom", new MapBuilder()
                                        .put("planownership.bamboo.plugin.plan.config.ownerOfBuild", "achystoprudov")
                                        .build())
                                .build()))
                .stages(new Stage("Default Stage")
                        .jobs(new Job("Fail Build Plugin",
                                new BambooKey("FBP"))
                                .artifacts(new Artifact()
                                                .name("Bamboo Logs")
                                                .copyPattern("**/atlassian-bamboo.log*")
                                                .location("plugin/target")
                                                .required(),
                                        new Artifact("Webdriver screenshots")
                                                .location("plugin/target")
                                                .copyPattern("webdriverTests/**")
                                                .shared(false))
                                .requirements(new Requirement("os")
                                        .matchType(Requirement.MatchType.EQUALS)
                                        .matchValue("Linux"))
                                .tasks(new VcsCheckoutTask()
                                                .checkoutItems(new CheckoutItem()
                                                                .repository(new VcsRepositoryIdentifier()
                                                                        .name("Bamboo Master on Stash"))
                                                                .path("bamboo"),
                                                        new CheckoutItem()
                                                                .repository(new VcsRepositoryIdentifier()
                                                                        .name("Bamboo Fail Trigger"))
                                                                .path("plugin")),
                                        new ScriptTask()
                                                .description("Install Firefox")
                                                .inlineBody("#!/bin/sh\n\nsudo apt-get -y -q --no-install-recommends install firefox libasound2 libgtk2.0-0\n"),
                                        new ScriptTask()
                                                .description("Checkout proper branch of plugin")
                                                .enabled(false)
                                                .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                                                .inlineBody("hg pull -b ${bamboo.dotnet.plugin.branch}\nhg update ${bamboo.dotnet.plugin.branch}")
                                                .workingSubdirectory("plugin"),
                                        new MavenTask()
                                                .description("Build Bamboo SNAPSHOT")
                                                .goal("${bamboo.bambooQuickCompileMavenGoals}")
                                                .environmentVariables("MAVEN_OPTS=\"${bamboo.funcMavenOpts}\"")
                                                .jdk("JDK 1.8")
                                                .executableLabel("Maven 3.2")
                                                .workingSubdirectory("bamboo"),
                                        new AnyTask(new AtlassianModule("com.atlassian.bamboo.plugins.variable.updater.variable-updater-generic:variable-extractor"))
                                                .description("Extract correct Bamboo version")
                                                .configuration(new MapBuilder()
                                                        .put("overrideCustomised", "")
                                                        .put("variable", "bambooVersion")
                                                        .put("removeSnapshot", "")
                                                        .put("includeGlobals", "")
                                                        .put("variableScope", "JOB")
                                                        .put("pomFile", "bamboo/pom.xml")
                                                        .build()),
                                        new MavenTask()
                                                .description("test plugin against Bamboo SNAPSHOT")
                                                .goal("clean install ${bamboo.mavenArgs} -Dbamboo.version=${bamboo.bambooVersion}")
                                                .environmentVariables("MAVEN_OPTS=\"${bamboo.funcMavenOpts}\"")
                                                .jdk("JDK 1.8")
                                                .executableLabel("Maven 3.2")
                                                .hasTests(true)
                                                .testResultsPath("**/surefire-reports/*.xml")
                                                .workingSubdirectory("plugin"))))
                .linkedRepositories("Bamboo Master on Stash",
                        "Bamboo Fail Trigger")
                .triggers(new ScheduledTrigger()
                        .cronExpression("0 0 1 ? * *"))
                .planBranchManagement(new PlanBranchManagement()
                        .delete(new BranchCleanup())
                        .notificationForCommitters())
                .notifications(new Notification()
                        .type(new PlanStatusChangedNotification())
                        .recipients(new UserRecipient("achystoprudov")));
    }

    public static void main(String... argv) {
        //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("https://tardigrade-bamboo.internal.atlassian.com");
        final PlanSpec planSpec = new PlanSpec();

        final Plan plan = planSpec.plan();
        bambooServer.publish(plan);
    }
}